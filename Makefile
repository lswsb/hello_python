DOCKER_USERNAME := lschwsbpl
TAG_VERSION=$(if $(BITBUCKET_TAG),${BITBUCKET_TAG},1.0.0)
TAG=$(DOCKER_USERNAME)/hello-world-printer:${TAG_VERSION}

deps:
	pip install -r requirements.txt; \
	pip install -r test_requirements.txt
lint:
	flake8 hello_world test
test:
	PYTHONPATH=. pytest --verbose -s
run:
	python main.py
.PHONY: test
docker_build:
	docker build -t hello-world-printer .
docker_run: docker_build
	docker run 	--name hello-world-printer-dev 	-p 5000:5000 	-d hello-world-printer
docker_push: docker_build 
	@docker login --username $(DOCKER_USERNAME) --password ${DOCKER_PASSWORD}
	docker tag hello-world-printer $(TAG)
	docker push $(TAG)
	docker logout